package pos.machine;

public class Good {
    private final String barcode;
    private final String name;
    private final int price;

    private int count;

    public Good(String barcode, String name, int price,int count) {
        this.barcode = barcode;
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getCount(){return count;}


}
