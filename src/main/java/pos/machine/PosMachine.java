package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        return createBillByBarcode(barcodes);
    }

    public boolean isEmpty(List<String> barcodes){
        return barcodes==null && barcodes.size()==0;
    }

    public Map<String,Integer> getBarcodeMap(List<String> barcodes){
        Map<String,Integer> barcodesMap = new HashMap<>();
        for (String barcode : barcodes) {
            if(barcodesMap.containsKey(barcode)){
                barcodesMap.put(barcode,barcodesMap.get(barcode)+1);
            }else{
                barcodesMap.put(barcode,1);
            }
        }
        return barcodesMap;
    }

    public Map<String,Good> getGoodsMap(List<String> barcodes){
        Map<String,Integer> barcodeMap =getBarcodeMap(barcodes);
        List<Item> items= ItemsLoader.loadAllItems();
        List<String> barcodeList =new ArrayList<>();
        Map<String,Good> goods = new HashMap<>();
        for (Item item : items) {
            barcodeList.add(item.getBarcode());
        }
        for (String s : barcodeMap.keySet()) {
            if(barcodeList.contains(s)){
                Item item = items.get(barcodeList.indexOf(s));
                goods.put(s,new Good(s,item.getName(),item.getPrice(),barcodeMap.get(s)));
            }
        }
        return goods;
    }

    public String generateBill(List<String> noRepectBarcodeList,Map<String,Good> goods){
        int sum=0;
        StringBuffer bill =new StringBuffer();
        bill.append("***<store earning no money>Receipt***");
        bill.append("\n");
        for (String s : noRepectBarcodeList) {
            Good good=goods.get(s);
            int money=good.getCount()*good.getPrice();
            bill.append(String.format("Name: %s, Quantity: %d, Unit price: %d (yuan), Subtotal: %d (yuan)",good.getName(),good.getCount(),good.getPrice(),money));
            bill.append("\n");
            sum+=money;
        }
        bill.append("----------------------");
        bill.append("\n");
        bill.append(String.format("Total: %d (yuan)",sum));
        bill.append("\n");
        bill.append("**********************");
        return bill.toString();
    }

    public String makeBill(List<String> barcodes){
        List<String> noRepectBarcodeList = barcodes.stream().distinct().collect(Collectors.toList());
        Map<String,Good> goods = getGoodsMap(barcodes);
        return generateBill(noRepectBarcodeList,goods);
    }

    public String createBillByBarcode(List<String> barcode){
        return isEmpty(barcode) ? null : makeBill(barcode);
    }

}
